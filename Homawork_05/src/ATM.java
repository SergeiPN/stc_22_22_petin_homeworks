public class ATM {
    private int balance;
    private int maxAmaut;
    private int maxBalance;
    private int sumOperations;

    ATM(int maxAmaut, int maxBalance, int balance) {
        this.balance = balance;
        this.maxAmaut = maxAmaut;
        this.maxBalance = maxBalance;
    }

    public ATM() {
    }

    public int withdrawal(int sumWithdrawal) {
        this.sumOperations++;
        if (sumWithdrawal <= balance && sumWithdrawal <= maxAmaut) {
            this.balance = balance - sumWithdrawal;
            return sumWithdrawal;
        }
        return 0;
    }

    public int depositing(int sumDepositing) {
        this.sumOperations++;
        if (sumDepositing + balance > maxBalance) {
            int oldBalance = balance;
            balance = maxBalance;
            return sumDepositing + oldBalance - maxBalance;
        }
        return 0;
    }

    public int getBalance() {
        return balance;
    }
}


