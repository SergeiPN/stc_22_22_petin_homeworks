import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int length = scanner.nextInt();

        int[] array = new int[length];

        int localMin = 0;

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        if (array[array.length - 1] > array[0] && array[0] < array[1]) {
            localMin++;
        } else if (array[array.length - 1] < array[0] && array[array.length - 1] < array[array.length - 2]) {
            localMin++;
        }

        for (int i = 1; i < array.length - 1; i++) {
            if (array[i - 1] > array[i] && array[i] < array[i + 1]) {
                localMin++;
            }
        }

        System.out.println("количество локальных минимума - " + localMin);

    }
}