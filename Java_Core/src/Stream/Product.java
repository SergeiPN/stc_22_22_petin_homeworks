package Stream;

public class Product {
    private Integer id;
    private String name;
    private Double cost;
    private int count;

    public Product(Integer id, String name, Double cost, int count) {
        this.id = id;
        this.name = name;
        this.cost = cost;
        this.count = count;
    }

    public String toString() {
        return "Product {" + "id=" + id + ", name='" + name + '\'' + ", cost=" + cost + ", count=" + count + '}';
    }

    public Integer id() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String name() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double cost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public int count() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
