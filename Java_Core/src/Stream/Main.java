package Stream;

import java.io.IOException;

public class Main {
   public static void main(String[] args) {
      ProductRepository productRepository = new ProductsRepositoryFileBasedImpl("C:\\Users\\User\\Desktop\\stc_22_22_petin_homeworks\\Java_Core\\src\\Stream\\input");
      try {
         Product milk = productRepository.findById(1);
         System.out.println(milk);
         System.out.println("===========");
         System.out.println(productRepository.findAllByTitleLike("ол"));
         System.out.println("==========");
         milk.setCost(1000.0);
         productRepository.update(milk);
         System.out.println(productRepository.findById(1));
      } catch (IOException ex) {
         System.out.println("Error");
      }
   }
}