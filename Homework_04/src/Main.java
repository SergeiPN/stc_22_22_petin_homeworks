
public class Main {

    public static int sum(int[] array, int from, int to) {

        int sum = 0;

        if (from > to || to > array.length) {
            return -1;
        }

        for (int i = from; i <= to; i++) {
            sum += array[i];
        }

        return sum;

    }

    public static void evenNumber(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                System.out.println(array[i]);
            }
        }
    }

    public static void main(String[] args) {    // проверка

        int[] x = {34, 10, 11, 56, 57, 93, 10, -3, 32, 15};

        int sum1 = sum(x, 9, 7);

        System.out.println(sum1);

        int sum2 = sum(x, 2, 6);

        System.out.println(sum2);

        evenNumber(x);

    }

}